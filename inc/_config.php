<?php

define("USER_LEVEL_USER",1);
define("USER_LEVEL_ADMIN",2);
define("USER_LEVEL_SUPERADMIN",3);

define("ACCOUNT_STATUS_INACTIVE", 0);
define("ACCOUNT_STATUS_PENDING", 1);
define("ACCOUNT_STATUS_ACTIVE", 2);

define("DOMAIN_STATUS_INACTIVE", 0);
define("DOMAIN_STATUS_PENDING", 1);
define("DOMAIN_STATUS_ACTIVE", 2);

define("ADMIN_HOME", "/admin/index.php");

$status = ['Inactive', 'Active'];
$userStatus = ['Inactive', 'Pending', 'Active'];
$statusAction = ['Activate', 'Activate', 'Deactivate'];

$errors = [];
$errors['err_exists'] = "This domain already exists in our database.";

$levels = ['user', 'admin', 'superadmin'];

function clean($input_str) {
  $return_str = str_replace(array('<',';','|','&','>',"'",'"',')','('), array('&lt;','&#58;','&#124;','&#38;','&gt;','&apos;','&#x22;','&#x29;','&#x28;'), $input_str);
  $return_str = str_ireplace('%3Cscript', '', $return_str);
  return $return_str;
}

// $servername = "with.cxj2lttefkke.ap-southeast-2.rds.amazonaws.com";
// $username = "withcollective";
// $password = "W1thC0ll3ct1v3";

$servername = "localhost";
$username = "cookies";
$password = "Password1";

$dbname = "cookies";

$link = mysqli_connect($servername, $username, $password);
if (!$link) {
  die('Could not connect: ' . mysqli_error($link));
}
$db_selected = mysqli_select_db($link, $dbname);
if (!$db_selected) {
  die('Cannot access' . DB_NAME . ': ' . mysqli_error($link));
}
?>