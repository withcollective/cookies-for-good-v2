<?php

function timediff($tim) {
  $sd = new DateTime(date('Y-m-d H:i:s', $tim));
  $ss = $sd->diff(new DateTime(date('Y-m-d H:i:s', time())));
  if ($tim > time()) {
    if ($ss->days > 732) {
      return " <span class=\"highlight notice\">Permanent</span>";
    }
    if ($ss->days >= 365) {
      return " (".$ss->y."y ".$ss->m."m ".$ss->d."d)";
    }
    if ($ss->days >= 30) {
      if ($ss->m > 0) {
        return " (".$ss->m."m ".$ss->d."d)";
      }else {
        return " (".$ss->d."d)";
      }
    }
    if ($ss->days < 30 && $ss->days > 0) {
      return " (".$ss->d."d ".$ss->h."h)";
    }
    if ($ss->days < 1 && $ss->h > 0) {
      return " (".$ss->h."hrs ".$ss->i."min)";
    }
    return " (".$ss->i."mins)";
  }else{
    return " <span class=\"highlight error\">Expired ".$ss->d."d ".$ss->h."h ago</span>";
  }
}

$sort = $_REQUEST['sort'];
$order = $_REQUEST['order'];

if ($sort=="") {
  $sort = "time_added";
}

$sortLabels = ['Domain', 'Contact name', 'Email address', 'Phone no.', 'Time added', 'Valid until', 'Status'];
$sortParams = ['domain', 'name', 'email', 'phone', 'time_added', 'valid_till', 'active'];
$status = ['Inactive', 'Pending', 'Active'];

$order = $order=="asc" ? "asc":"desc";
?>
<table class="list" cellspacing="0" cellpadding="0" width="100%">
  <thead>
    <tr>
      <?php
      foreach ($sortParams as $i=>$par) {
        if ($sort==$par) {
          $sortClass=' class="sorting-active sort-'.$order.'"';
          $nextOrder = $order=="asc" ? "desc":"asc";
          $activeSortClass = ' class="active-sort"';
        }else{
          $sortClass=' class="sort-desc"';
          $nextOrder = "asc";
          $activeSortClass = "";
        }
        echo '<th'.$activeSortClass.'>
                <a'.$sortClass.' href="admin.php?action='.$action.'&id='.$entryId.'&sort='.$sortParams[$i].'&order='.$nextOrder.'">'.$sortLabels[$i].'</a>
              </th>';
      } ?>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
  <?php
    while ($row = $result->fetch_assoc()) {
      $expired = ($row['valid_till'] > time()) ? 0 : 1;
      $sd = new DateTime(date('Y-m-d H:i:s', $row['valid_till']));
      $daydiff = $sd->diff(new DateTime(date('Y-m-d H:i:s', time())));
      $dd = $daydiff->days;
      echo '<tr>
        <td>'.$row['domain'].'</td>
        <td>'.$row['name'].'</td>
        <td>'.$row['email'].'</td>
        <td>'.$row['phone'].'</td>
        <td>'.date('d M Y @ H:i:s', $row['time_added']).'</td>
        <td>'.(($dd<=731 && $expired<1) ? date('d M Y @ H:i:s', $row['valid_till']) : '').timediff($row['valid_till']).'</td>
        <td class="'.strtolower($status[$row['active']]).'">'.$status[$row['active']].'</td>
        <td>';
          if ($row['level'] <= $_SESSION['user_level'] && $_SESSION['user_level'] > USER_LEVEL_USER){
            echo '<a class="a-icon a-edit" title="Edit" href="?action=edit&id='.$row['id'].'&sort='.$sort.'&order='.$order.'">Edit</a>';
                  // <a class="a-icon a-toggle a-'.(($row['active'] != 0)?'active':'inactive').'" title="'.(($row['active'] != 0)?'Deactivate':'Activate').'" href="?action=toggle&id='.$row['id'].'&sort='.$sort.'&order='.$order.'">'.(($row['active'] != 0)?'Deactivate':'Activate').'</a>';
            echo '<a class="a-icon a-toggle a-'.strtolower($status[$row['active']]).'" title="Toggle status" href="?action=toggle&id='.$row['id'].'&sort='.$sort.'&order='.$order.'">'.$status[$row['active']].'</a>';
          }
          echo '<a class="a-icon a-snippet" data-id="'.$row['id'].'" title="Generate snippet" href="#">Generate snippet</a>';
          if ( $_SESSION['user_level'] > USER_LEVEL_ADMIN){
            echo '<a onclick="return confirm(\'Are you sure?\');" class="a-icon a-delete" title="DELETE" href="?action=delete&id='.$row['id'].'&sort='.$sort.'&order='.$order.'">DELETE</a>';
          }
        echo '</td>
      </tr>';
    }
  ?>
  </tbody>
</table>