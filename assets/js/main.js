deleteCookie = () => {
  var now = new Date();
  now.setMonth(now.getMonth() - 1);
  document.cookie = "gdpr=false";
  document.cookie = "expires=" + now.toUTCString() + ";"
}
var serialize = function (form) {
  var serialized = [];
  for (var i = 0; i < form.elements.length; i++) {
    var field = form.elements[i];
    if (!field.name || field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') continue;
    if (field.type === 'select-multiple') {
      for (var n = 0; n < field.options.length; n++) {
        if (!field.options[n].selected) continue;
        serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.options[n].value));
      }
    }
    else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
      serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.value));
    }
  }
  return serialized.join('&');
};

validateEmail = (email) => {
  var re = /\S+@\S+\.\S+/;
  return re.test(email);
}

// Validate form 
var formErrors = [];
validateForm = (form) => {
  var isValid = true;
  formErrors = [];
  if (form.domain.value.trim() == "" || form.domain.value.trim().length < 4) {
    form.domain.classList.add('hasError')
    formErrors.push("<p>Domain name is not valid</p>")
    isValid = false;
  } else {
    form.domain.classList.remove('hasError')
  }
  if (form.name.value.trim() == "" || form.name.value.trim().length < 4) {
    form.name.classList.add('hasError')
    formErrors.push("<p>Name is not valid</p>")
    isValid = false;
  } else {
    form.name.classList.remove('hasError')
  }
  if (!validateEmail(form.email.value)) {
    form.email.classList.add('hasError')
    formErrors.push("<p>Email Address is not valid</p>")
    isValid = false;
  } else {
    form.email.classList.remove('hasError')
  }
  if (form.phone.value.trim() == "" || form.phone.value.trim().length < 4) {
    form.phone.classList.add('hasError')
    formErrors.push("<p>Phone Number is not valid</p>")
    isValid = false;
  } else {
    form.phone.classList.remove('hasError')
  }

  return isValid;
}

// CUT FROM THE SNIPPET:
// if("false"==(document.cookie.match(/^(?:.*;)?\s*gdpr\s*=\s*([^;]+)(?:.*)?$/)||[,null])[1]) {\n\
// ^^^ PUT BACK IF NEED BE - but it'll disable some hit tracking!!

generateSnippet = () => {
  // ---------------
  var errors = document.getElementById('errors')

  if (!validateForm(form)) {
    var errorHTML = formErrors.join("");

    errors.innerHTML = (errorHTML);
    errors.style.marginBottom = "100px";

    window.scrollTo(0, errors.offsetTop + errors.offsetHeight + 100);

    return false;
  }
  // ---------------

  let snippetCode;
  let ta = document.getElementById('result');
  let err = document.getElementById('errors');
  let succ = document.getElementById('success');
  let ldr = document.getElementById('loader');
  var ENV_PATH;
  if (document.location.host == "cookies") {
    ENV_PATH = "_local";
  } else if (document.location.host == "cookies.withcollective.com") {
    ENV_PATH = "_staging";
  } else {
    ENV_PATH = "";
  }
  var vars = serialize(form);
  var xmlhttp = new XMLHttpRequest();
  ldr.style.display = "inline-block";
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      // console.log('RESPONSE:', this.responseText);
      if (this.responseText.substring(0, 2) != "__") { // UNDERSCORES DESIGNATE ERROR MESSAGES
        snippetCode = '<script>\n\
  var c=document.createElement("link");\n\
    c.setAttribute("rel", "stylesheet");\n\
    c.setAttribute("type", "text/css");\n\
    c.setAttribute("href", "https://s3-ap-southeast-2.amazonaws.com/with-collective-assets/css/style.css");\n\
  document.getElementsByTagName("head")[0].appendChild(c);\n\
  var j=document.createElement("script");\n\
    j.setAttribute("src", "https://s3-ap-southeast-2.amazonaws.com/with-collective-assets/js/c'+ ENV_PATH + '.js");\n\
    j.setAttribute("data-uid", "'+ this.responseText + '");\n\
  document.getElementsByTagName("head")[0].appendChild(j);\n\
</script>';
        ta.value = snippetCode;
        document.getElementById("result-wrapper").style.display = "block";
        succ.innerHTML = "<p>Domain successfully registered. Thank you!</p>";
        err.innerHTML = "";
      } else if (this.responseText == "__DOMAIN_EXISTS") {
        err.innerHTML = "<p>This domain entry already exists.</p>";
        succ.innerHTML = "";
      } else if (this.responseText == "__INSERT_FAIL") {
        err.innerHTML = "<p>Error inserting this database entry. Please try again later.</p>";
        succ.innerHTML = "";
      } else {
        err.innerHTML = "<p>An error occurred. Please try again later.</p>";
        succ.innerHTML = "";
      }
      ldr.style.display = "none";
    }
  };
  xmlhttp.open("POST", "store.php", true);
  xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xmlhttp.send(vars);
}

copyText = () => {
  let ta = document.getElementById("result");
  if (ta.value.length > 0) {
    ta.select();
    document.execCommand("copy");
    let res = document.getElementById("result-wrapper");
    let rp = res.querySelector('p');
    rp && res.removeChild(rp);
    let p = document.createElement('p');
    p.innerText = "Code copied to clipboard! Characters: " + ta.value.length;
    res.appendChild(p);
  }
}

function validate() {
  // console.log("VALIDATING: ");
  // var validation = 0;
  // if (document.form.domain.value.trim() == "") {
  //   validation++;
  // }
  // if (document.form.valid_till.value < 1) {
  //   validation++;
  // }
  // if (document.form.name.value.trim() == "") {
  //   validation++;
  // }
  // if (document.form.email.value.trim() == "") {
  //   validation++;
  // }
  // if (document.form.phone.value.trim() == "") {
  //   validation++;
  // }

  var validation = true;

  function checkInputHasContent(name, isNumeric) {
    // If input is not a text input, check for value
    if (document.form[name].value.trim() == "") {
      validation = false;
    }
  }

  checkInputHasContent('first_name');
  checkInputHasContent('last_name');
  checkInputHasContent('username');
  checkInputHasContent('address');
  checkInputHasContent('email');
  checkInputHasContent('phone');
  checkInputHasContent('password');
  checkInputHasContent('password2');

  // if passwords don't match, form is invalid.
  if (document.form.password.value !== document.form.password2.value) {
    validation = false;
  }

  if (!document.form.agreement.checked) {
    validation = false;
  }
  console.log(document.form.agreement.checked, document.form.agreement.value);



  $('input[type=submit]').attr('disabled', !validation);

}

function onFormInputChange() {
  document.getElementById('success').innerHTML = "";
  this.classList.remove('hasError');

  // console.log('name: ', this.name)

  // trim white space on every field, except those which may include multiple words.
  switch (this.name) {
    case "address":
    case "name":
    case "first_name":
    case "last_name":
    case "username":
      this.value = this.value.replace(/\s\s+/g, ' ');
      this.value = this.value.replace(new RegExp("[^0-9a-zA-Z_+\-:\.\'\s ]", "gm"), "");
      break;

    default:
      this.value = this.value.trim();
      break;
  }

  // Additional formatting
  switch (this.name) {
    // case "domain":
    //   this.value = this.value.replace(new RegExp("[^0-9a-zA-Z_+\-:\.]", "gm"), "");
    //   this.value = this.value.replace(new RegExp("[,/]", "gm"), "");
    //   break;

    case "email":
      this.value = this.value.replace(new RegExp("[^0-9a-zA-Z@_+\-:\.]", "gm"), "");
      this.value = this.value.replace(new RegExp("[,/]", "gm"), "");
      break;

    case "phone":
      this.value = this.value.replace(new RegExp("[^0-9+:.]", "gm"), "");
      break;

    default:
      break;
  }

  // resetErrorWindow();
}

function onInputBlur() {
  // check passwords match
  if (this.name === "password" || this.name === "password2") {
    if ($("#password").val() != $("#password2").val()) {
      console.log("Password field mismatch");
      $("#password").addClass('hasError');
      $("#password2").addClass('hasError');
    } else {
      $("#password").removeClass('hasError');
      $("#password2").removeClass('hasError');
    }
  }
}

$(function () {
  $('.select2rep').select2({
    minimumResultsForSearch: -1
  });
  $('#form input').keyup(function () {
    $('#errors').html('');
    $('textarea').val('');
    return validate();
  });
  $('.select_period').change(function () {
    var len = $('option:selected', $(this)).attr('rel');
    $('#sublen').val(len);
  });

  $('#form input').change(onFormInputChange)
  $('#form input').keyup(onFormInputChange)
  $(document).on("change", "input[name='agreement']", function () {
    // do checkbox test here
    $('input[type=submit]').attr('disabled', !document.form.agreement.checked);
    validate();
  });
  $('#form input').blur(onInputBlur)
});
