$(function(){
  $('#domain_select').select2();

  $('.select2rep').select2({
    minimumResultsForSearch: -1
  });

  $('#domain_select').change(function(){
    var val = $(this).select2('val');
    if (val !== "-1") {
      document.location.href="?action=stats&stats_domain="+val;
    }
  });

  $('.select_period').change(function(){
    var len = $('option:selected', $(this)).attr('rel');
    $('#sublen').val(len);
  });
});

(function(){
  var gs = document.querySelectorAll('.a-snippet');
  Array.from(gs).forEach(function(g) {
    g.addEventListener('click', function(e){
      e.preventDefault();
      var sl = document.querySelector('#snippet');
      if (sl) {
        sl.parentElement.removeChild(sl);
      }
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
          // console.log('RESPONSE:', this.responseText);
          if (this.responseText.indexOf('|') > -1) {
            let resp = this.responseText.split('|');
            snippetCode = '<script>\n\
if(!document.cookie.match(/^(?:.*;)?s*gdprs*=s*([^;]+)(?:.*)?$/) || "false"==(document.cookie.match(/^(?:.*;)?s*gdprs*=s*([^;]+)(?:.*)?$/)||[,null])[1]) {\n\
  var c=document.createElement("link");\n\
    c.setAttribute("rel", "stylesheet");\n\
    c.setAttribute("type", "text/css");\n\
    c.setAttribute("href", "https://s3-ap-southeast-2.amazonaws.com/with-collective-assets/css/style.css");\n\
  document.getElementsByTagName("head")[0].appendChild(c);\n\
  var j=document.createElement("script");\n\
    j.setAttribute("src", "https://s3-ap-southeast-2.amazonaws.com/with-collective-assets/js/c2.js");\n\
    j.setAttribute("data-id", "'+resp[0]+'");\n\
    j.setAttribute("data-id2", "'+resp[1]+'");\n\
  document.getElementsByTagName("head")[0].appendChild(j);\n\
}\n\
</script>';
            var snippetLayer = document.createElement('div');
            snippetLayer.setAttribute('id', 'snippet_overlay');
            var snippetContent = document.createElement('div');
            snippetContent.setAttribute('id', 'snippet');
            var snippetText = document.createElement('textarea');
            snippetText.setAttribute('id', 'result');
            snippetText.setAttribute('readonly', 'readonly');
            snippetContent.appendChild(snippetText);
            var snippetButton = document.createElement('button');
            snippetButton.setAttribute('class', 'btn');
            snippetButton.innerHTML = "Copy &amp; close";
            snippetContent.appendChild(snippetButton);

            document.querySelector('content').appendChild(snippetLayer);
            g.parentElement.appendChild(snippetContent);

            snippetText.value = snippetCode;
            snippetButton.addEventListener('click', function() {
              snippetText.select();
              document.execCommand("copy");
              snippetLayer.parentElement.removeChild(snippetLayer);
              snippetContent.parentElement.removeChild(snippetContent);
            });
            snippetLayer.addEventListener('click', function(){
              snippetLayer.parentElement.removeChild(snippetLayer);
              snippetContent.parentElement.removeChild(snippetContent);
            });
          }else{
            // ERROR HANDLER
          }
        }
      };
      var vars = "id="+g.dataset.id+"&id2="+g.dataset.id2;
      xmlhttp.open("POST", document.location.protocol+"//"+document.location.host+"/admin/scripts/check-uid.php", true);
      xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
      xmlhttp.send(vars);
    });
  });

  var errs = document.querySelector('.error-message');
  if (errs) {
    setTimeout(function(){
      errs.classList.remove('popup');
    }, 4000);
    setTimeout(function(){
      errs.parentElement.removeChild(errs);
    }, 5000);
  }
})();