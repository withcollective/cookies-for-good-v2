<?php
  require('inc/_config.php');

  $uid = $_REQUEST['uid'];
  $query = "SELECT * FROM `users` WHERE `uid`='".$uid."' && `status`=1";
  $_content = "";
  $_title = "Account activation | Cookies For Good";

  if ($result = $link->query($query)) {
    while ($row = $result->fetch_assoc()) {
      if ($row['id'] > 0) {
        $uquery = "UPDATE `users` SET `status`=2 WHERE `uid`='".$uid."' && `status`=1";
        $res = $link->query($uquery);
        if ($res) {
          $_content = '<h1>Your account has been activated!</h1><p>Thank you for using Cookies For Good.</p>
                      <p>Please continue by logging in to the Administration and adding your domains.</p>
                      <p><a class="btn" href="/index.php"><i class="icon-login"></i> Log in</a></p>';
          $_title = "Account activated! | Cookies For Good";
        }else{
          $_content = '<h1>Error occured</h1><p>Please try again later or contact the administrator.</p>';
          $_title = "Error | Cookies For Good";
        }
      }else {
        $_content = '<h1>Error occured</h1><p>Please try again later or contact the administrator.</p>';
        $_title = "Error | Cookies For Good";
      }
    }
    $result->free();
  }
  mysqli_close($link);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="Expires" content="0">
  <link rel='shortcut icon' type='image/x-icon' href='cookie.ico' />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" type="text/css" media="screen" href="assets/css/main.css">
  <title><?=$_title?></title>
</head>
<body>
  <div class="activation">
    <?=$_content?>
  </div>
</body>
</html>