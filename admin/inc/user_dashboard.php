<?php
$domainsQuery = "SELECT * FROM `domains` WHERE `owner_id`=".$_SESSION['user_id']." ORDER BY `id` DESC"; 
$domainsResult = $link->query($domainsQuery);
$dcount = mysqli_num_rows($domainsResult);

$userQuery = "SELECT * FROM `users` WHERE id=".$_SESSION['user_id'];
$userResult = $link->query($userQuery);
$user = $userResult->fetch_assoc();

?>
<h2>Dashboard</h2>
<hr />
<div class="dashboard-content user-dashboard">
  <div class="user-info">
    <h3>My details</h3>
    <table cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <th>Subscription info:</th>
          <td><?=$user['sub_length']?> months, <?=$user['max_domains']?> domains</td>
        </tr>
        <tr>
          <th>Subscription expiry:</th>
          <td><?=date('d M Y @ H:i:s', $user['valid_till'])." (".timediff($user['valid_till']).")"?></td>
        </tr>
        <tr>
        <th>Created:</th>
          <td><?=date('d M Y @ H:i:s', $user['time_created'])." (".timeago($user['time_created']).")"?></td>
        </tr>
        <tr>
          <th>Last login:</th>
          <td><?=(($user['last_login'] > 0) ? date('d M Y @ H:i:s', $user['last_login']) : 'never')?></td>
        <tr>
          <th>Name:</th>
          <td><?=$user['name']?></td>
        </tr>
        <tr>
          <th>Billing address:</th>
          <td><?=$user['address']?></td>
        </tr>
        <tr>
          <th>Email address:</th>
          <td><?=$user['email']?></td>
        </tr>
        <tr>
          <th>Phone number:</th>
          <td><?=$user['phone']?></td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="domain-info">
    <h3>
      <?php if ($dcount < $user['max_domains']):?>
        <a class="btn" href="/admin/index.php?action=add"><i class="icon-plus"></i> Add new</a>
      <?php else:?>
        <a class="btn highlight" href="#">Upgrade subscription</a>
      <?php endif; ?>
      My domains (<?=$dcount?>/<?=$user['max_domains']?>)
    </h3>

    <?php if ($msg!=""):?>
      <div class="error-message popup"><?=$errors[$msg]?></div>
    <?php endif; ?>

    <?php if ($action == "add") :?>
      <form method="post" action="?action=insert" onsubmit="validateForm(this)">
        <div class="form-group">
          <label><span>Domain name</span> <input id="add_domain" required type="text" name="domain" /></label>
        </div>
        <script>
          document.getElementById('add_domain').focus();
        </script>
        <div class="form-group button-group">
          <button class="btn" type="submit"><i class="icon-plus"></i> Add</button>
          <a class="btn" href="<?=ADMIN_HOME?>?action=overview&sort=<?=$sort?>&order=<?=$order?>"><i class="icon-cancel-circled"></i> Cancel</a>
        </div>
      </form>
    <?php endif;?>

    <?php if ($action == "edit" && $entryId > 0) :?>
      <form method="post" action="?action=update&id=<?=$entryId?>" onsubmit="validateForm(this)">
        <div class="form-group">
          <label><span>Domain name</span> <input required type="text" id="edit_domain" name="domain" value="<?=$erow['domain_name']?>" /></label>
        </div>
        <script>
          document.getElementById('edit_domain').focus();
        </script>
        <div class="form-group button-group">
          <input type="hidden" name="time_added" value="<?=$erow['time_added']?>" />
          <button class="btn" type="submit"><i class="icon-pencil"></i> Update</button>
          <a class="btn" href="<?=ADMIN_HOME?>?action=overview&sort=<?=$sort?>&order=<?=$order?>"><i class="icon-cancel-circled"></i> Cancel</a>
        </div>
      </form>
      <hr />
    <?php endif;?>

    <table width="100%" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th>Domain name</th>
          <th>Status</th>
          <th>Hits</th>
          <th>Added</th>
          <th width="20%">Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php if (mysqli_num_rows($domainsResult) > 0) {
        while ($drow = $domainsResult->fetch_assoc()) {
            $hitsQuery = "SELECT COUNT(*) as total FROM `hits` WHERE `ref_domain`='".$drow['domain_name']."'";
            $hitsResult = $link->query($hitsQuery);
            $hits = $hitsResult->fetch_assoc();
          ?>
          <tr>
            <td><strong><?=$drow['domain_name']?></strong></td>
            <td><?=$status[$drow['status']]?></td>
            <td><?=$hits['total']?></td>
            <td><?=date('d M Y @ H:i:s', $drow['time_added'])?></td>
            <td>
            <?php
              // echo '<a class="a-icon a-edit" title="Edit" href="?action=edit&id='.$drow['id'].'&sort='.$sort.'&order='.$order.'">Edit</a>';
              echo '<a class="a-icon a-toggle a-'.strtolower($status[$drow['status']]).'" title="Toggle status" href="?action=toggle&id='.$drow['id'].'&sort='.$sort.'&order='.$order.'">'.$status[$row['status']].'</a>';
              echo '<a class="a-icon a-snippet" data-id="'.$drow['uid'].'" data-id2="'.$_SESSION['user_uid'].'" title="Generate snippet">Generate snippet</a>';
              echo '<a onclick="return confirm(\'Are you sure?\');" class="a-icon a-delete" title="DELETE" href="?action=delete&id='.$drow['id'].'&sort='.$sort.'&order='.$order.'">DELETE</a>';
            ?>
            </td>
          </tr>
          <?php }
        }else{
          echo '<tr><td colspan="10" align="center">No domains have been registered yet.</td></tr>';
        }
         ?>
      </tbody>
    </table>
  </div>
</div>