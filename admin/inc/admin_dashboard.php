<?php
$userQuery = "SELECT * FROM `users` WHERE id=".$_SESSION['user_id'];
$userResult = $link->query($userQuery);
$user = $userResult->fetch_assoc();
$users = [];
$uids = [];

$userListQuery = "SELECT * FROM `users` WHERE `user_level`<2 ORDER BY `id` DESC";
$userListResult = $link->query($userListQuery);

?>
<h2>Admin Dashboard</h2>
<hr />
<div class="dashboard-content admin-dashboard">
  <div class="users-col">
    <h4>
      <span>Select user:</span>
      <a class="btn" href="/admin/index.php?action=adduser"><i class="icon-plus"></i> Add new</a>
    </h4>
    <div class="users-wrapper">
      <div class="user">
        <?php while($urow=$userListResult->fetch_assoc()) {
          $dcquery = "SELECT COUNT(*) as total FROM `domains` WHERE `owner_id`='".$urow['id']."'";
          $dcres = $link->query($dcquery);
          $domainCount = $dcres->fetch_assoc()['total'];
          $expired = ($row['valid_till'] > time()) ? 0 : 1;
          $sd = new DateTime(date('Y-m-d H:i:s', $row['valid_till']));
          $daydiff = $sd->diff(new DateTime(date('Y-m-d H:i:s', time())));
          $dd = $daydiff->days;
          $users[$entryId] = $urow['name'];
          $uids[$entryId] = $urow['uid'];
        ?>
          <a href="?action=detail&userid=<?=$urow['id']?>" class="user-item<?=($entryId==$urow['id'])? ' user-selected' : '';?>"><span class="user-status <?=strtolower($userStatus[$urow['status']])?>"></span><?=$urow['name']?> <small>(<?=$userStatus[$urow['status']]?>, <?=$domainCount?> domains)</small></a>
        <?php } ?>
      </div>
    </div>
  </div>
  <div class="domains-col">
    <div class="user-domains">
      <?php if ($userId < 1) :?>
        <p class="empty-message">Please select a user from the list.</p>
      <?php else:
        $accountQuery = "SELECT * FROM `users` WHERE `id`=".$userId;
        $accResult = $link->query($accountQuery);
        $arow = $accResult->fetch_assoc();

        if ($arow['status']<2) {
          $ustatusicon = "ok-circled";
        }else{
          $ustatusicon = "cancel-circled";
        }
      ?>
          <h4>
            <span>User details</span>
            <a class="a-edit" href="?action=edituser&userid=<?=$userId?>"><i class="icon-pencil"></i> Edit user</a>
            <a href="?action=toggleuser&userid=<?=$userId?>" class="a-toggle a-<?=strtolower($userStatus[$arow['status']])?>"><i class="icon-<?=$ustatusicon?>"></i> <?=$statusAction[$arow['status']]?> user</a>
            <a onclick="return confirm('Are you sure?');" href="?action=deleteuser&userid=<?=$userId?>" class="a-delete"><i class="icon-trash"></i> Delete user</a>
          </h4>
          <table cellpadding="0" cellspacing="0" class="user-details-table">
            <tbody>
              <tr>
                <th>Subscription info:</th>
                <td><?=$arow['sub_length']?> months, <?=$arow['max_domains']?> domains</td>
              </tr>
              <tr>
                <th>Subscription expiry:</th>
                <td><?=date('d M Y @ H:i:s', $arow['valid_till'])." (".timediff($arow['valid_till']).")"?></td>
              </tr>
              <tr>
                <th>Created:</th>
                <td><?=date('d M Y @ H:i:s', $arow['time_created'])." (".timeago($arow['time_created']).")"?></td>
              </tr>
              <tr>
                <th>Last login:</th>
                <td><?=(($arow['last_login'] > 0) ? date('d M Y @ H:i:s', $arow['last_login']) : 'never')?></td>
              <tr>
                <th>Name:</th>
                <td><?=$arow['name']?></td>
              </tr>
              <tr>
                <th>Billing address:</th>
                <td><?=$arow['address']?></td>
              </tr>
              <tr>
                <th>Email address:</th>
                <td><?=$arow['email']?></td>
              </tr>
              <tr>
                <th>Phone number:</th>
                <td><?=$arow['phone']?></td>
              </tr>
            </tbody>
          </table>

          <?php if ($action == "edituser" && $userId > 0) :?>
            <div class="edit-user">
              <form action="?action=updateuser&userid=<?=$userId?>" method="post">
                <div class="form-group">
                  <label><span>Full name</span><input type="text" name="full_name" required value="<?=$eurow['name']?>" /></label>
                </div>
                <div class="form-group dual">
                  <label><span>Username</span><input type="text" name="username" required value="<?=$eurow['username']?>" readonly="readonly" /></label>
                  <label><span>Change password</span><input class="userpass" type="text" name="password" /></label>
                </div>
                <div class="form-group dual">
                  <label><span>Subscription type</span>
                    <select name="valid_till" class="select2rep select_period">
                      <option value="15552000" rel="6">6 months</option>
                      <option value="31536000" rel="12" selected>1 year</option>
                      <option value="63072000" rel="24">2 years</option>
                      <option value="999999999" rel="99999">permanent</option>
                    </select>
                    <input type="hidden" id="sublen" name="sub_length" value="12" />
                  </label>
                  <label><span>Max domains</span>
                    <select name="max_domains" class="select2rep select_max">
                      <option value="5">5</option>
                      <option value="10">10</option>
                      <option value="25">25</option>
                      <option selected value="50">50</option>
                      <option value="100">100</option>
                      <option value="9999">101+</option>
                    </select>
                  </label>
                <!-- </div> -->
                <?php if($eurow['user_level'] < 3): ?>
                  <!-- <div class="form-group"> -->
                    <label><span>Access level</span>
                      <select name="level" class="select2rep">
                          <option<?php if($eurow['user_level']==1){echo' selected="selected"';}?> value="1">User</option>
                        <?php if ($_SESSION['user_level'] > USER_LEVEL_USER): ?>
                          <option<?php if($eurow['user_level']==2){echo' selected="selected"';}?> value="2">Admin</option>
                        <?php endif; ?>
                        <?php if ($_SESSION['user_level'] > USER_LEVEL_ADMIN): ?> <!-- CHECK FOR THE FIRST SUPERADMIN TO PREVENT ZERO SUPERADMIN RIGHTS -->
                          <option<?php if($eurow['user_level']==3){echo' selected="selected"';}?> value="3">Superadmin</option>
                        <?php endif; ?>
                      </select>
                    </label>
                  </div>
                <?php endif; ?>
                <div class="form-group button-group">
                  <button class="btn" type="submit"><i class="icon-pencil"></i> Update user</button>
                  <a class="btn" href="?action=detail&userid=<?=$userId?>"><i class="icon-cancel-circled"></i> Cancel</a>
                </div>
                <div class="errors"><?=$_error?></div>
              </form>
            </div>
          <?php endif; ?>

          <hr />
          <h4>Registered domains for <?=$users[$entryId]?></h4>

          <?php if ($action == "editdomain" && $entryId > 0) :?>
            <hr />
            <form method="post" action="?action=updatedomain&id=<?=$entryId?>&userid=<?=$userId?>" onsubmit="validateForm(this)">
              <div class="form-group">
                <label><span>Domain name</span> <input required type="text" id="edit_domain" name="domain" value="<?=$erow['domain_name']?>" /></label>
              </div>
              <script>
                document.getElementById('edit_domain').focus();
              </script>
              <div class="form-group button-group">
                <input type="hidden" name="time_added" value="<?=$erow['time_added']?>" />
                <button class="btn" type="submit"><i class="icon-pencil"></i> Update</button>
                <a class="btn" href="<?=ADMIN_HOME?>?action=detail&userid=<?=$userId?>"><i class="icon-cancel-circled"></i> Cancel</a>
              </div>
            </form>
          <?php endif;?>

          <table cellpadding="0" cellspacing="0" width="100%">
            <thead>
              <th>Domain name</th>
              <th>Status</th>
              <th>Hits</th>
              <th>Added</th>
              <th width="20%">Actions</th>
            </thead>
            <tbody>
            <?php if ($userDomainsCount > 0) :
              while($udrow = $userDomainsResult->fetch_assoc()) :
                $hitsQuery = "SELECT COUNT(*) as total FROM `hits` WHERE `ref_domain`='".$udrow['domain_name']."'";
                $hitsResult = $link->query($hitsQuery);
                $hits = $hitsResult->fetch_assoc();
              ?>
                <tr<?=(($entryId == $udrow['id'])?' class="highlight"':'')?>>
                  <td><?=$udrow['domain_name']?></td>
                  <td><?=$status[$udrow['status']]?></td>
                  <td><?=$hits['total']?></td>
                  <td><?=date('d M Y @ H:i:s', $udrow['time_added'])?></td>
                  <td>
                  <?php
                    // echo '<a class="a-icon a-edit" title="Edit" href="?action=editdomain&userid='.$arow['id'].'&id='.$udrow['id'].'&sort='.$sort.'&order='.$order.'">Edit</a>';
                    echo '<a class="a-icon a-toggle a-'.strtolower($status[$udrow['status']]).'" title="Toggle status" href="?action=toggledomain&userid='.$arow['id'].'&id='.$udrow['id'].'&sort='.$sort.'&order='.$order.'">'.$status[$udrow['status']].'</a>';
                    echo '<a class="a-icon a-snippet" data-id="'.$udrow['uid'].'" data-id2="'.$uids[$entryId].'" title="Generate snippet">Generate snippet</a>';
                    if ($_SESSION['user_level'] > USER_LEVEL_ADMIN) {
                      echo '<a onclick="return confirm(\'Are you sure?\');" class="a-icon a-delete" title="DELETE" href="?action=deletedomain&id='.$udrow['id'].'&sort='.$sort.'&order='.$order.'">DELETE</a>';
                    }
                  ?>
                  </td>
                </tr>
                <?php endwhile;?>
              <?php else: ?>
                <?php echo '<tr><td colspan="10">No registered domains</td></tr>';?>
              <?php endif; ?>
            </tbody>
          </table>
      <?php endif; ?>
    </div>
  </div>
</div>
