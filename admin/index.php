<?php 
session_start();
require('../inc/_config.php');

if ($_SESSION['user_id'] < 1) {
  header('Location: /index.php');
}

function timeago($tim) {
  $seconds_ago = (time() - $tim);
  if ($tim > 0) {
    if ($seconds_ago >= 31536000) {
      return intval($seconds_ago / 31536000) . " years ago";
    } elseif ($seconds_ago >= 2419200) {
      return intval($seconds_ago / 2419200) . " months ago";
    } elseif ($seconds_ago >= 86400) {
      return  intval($seconds_ago / 86400) . " days ago";
    } elseif ($seconds_ago >= 3600) {
      return intval($seconds_ago / 3600) . " hours ago";
    } elseif ($seconds_ago >= 60) {
      return intval($seconds_ago / 60) . " minutes ago";
    } else {
      return "<1 minute ago";
    }
  }else{
    return 'never';
  }
}

function timediff($tim) {
  $sd = new DateTime(date('Y-m-d H:i:s', $tim));
  $ss = $sd->diff(new DateTime(date('Y-m-d H:i:s', time())));
  if ($tim > time()) {
    if ($ss->days > 732) {
      return " <span class=\"highlight notice\">Permanent</span>";
    }
    if ($ss->days >= 365) {
      return $ss->y."y ".$ss->m."m ".$ss->d."d";
    }
    if ($ss->days >= 30) {
      if ($ss->m > 0) {
        return $ss->m."m ".$ss->d."d";
      }else {
        return $ss->d."d)";
      }
    }
    if ($ss->days < 30 && $ss->days > 0) {
      return $ss->d."d ".$ss->h."h";
    }
    if ($ss->days < 1 && $ss->h > 0) {
      return $ss->h."hrs ".$ss->i."min";
    }
    return $ss->i."mins";
  }else{
    return " <span class=\"highlight error\">Expired ".$ss->d."d ".$ss->h."h ago</span>";
  }
}

$action = clean($_REQUEST['action']);
$entryId = clean($_REQUEST['id']);
$userId = clean($_REQUEST['userid']);
$domain = clean($_REQUEST['domain']);
$name = clean($_REQUEST['name']);
$email = clean($_REQUEST['email']);
$phone = clean($_REQUEST['phone']);
$valid_till = clean($_REQUEST['valid_till']);
$time_added = clean($_REQUEST['time_added']);
$full_name = clean($_REQUEST['full_name']);
$username = clean($_REQUEST['username']);
$password = clean($_REQUEST['password']);
$level = clean($_REQUEST['level']);
$sort = clean($_REQUEST['sort']);
$order = clean($_REQUEST['order']);
$max_domains = clean($_REQUEST['max_domains']);
$sub_length = clean($_POST['sub_length']);
$stats_domain = clean($_REQUEST['stats_domain']);
$msg = clean($_REQUEST['msg']);

$hit_description = ['', 'Mismatched domain', 'Domain inactive', 'Wrong UID', 'Not found', 'Domain pending', 'Domain expired'];
$_error = "";

$orderQuery = " ORDER BY `time_added` DESC";
$sortOrder="";

if ($sort!="") {
  $sortOrder = ($order=="asc") ? 'ASC':'DESC';
  $orderQuery = " ORDER BY `".$sort."` ".$sortOrder;
}


switch($action) {
  case 'detail':
    if ($userId>0) {
      $userDomainsQuery = "SELECT * FROM `domains` WHERE `owner_id`=".$userId." ORDER BY `id` DESC";
      $userDomainsResult = $link->query($userDomainsQuery);
      $userDomainsCount = mysqli_num_rows($userDomainsResult);
    }
    break;
  case 'logout':
    session_destroy();
    header('Location: /index.php');
    exit;
  case 'editdomain';
    if ($userId>0) {
      $userDomainsQuery = "SELECT * FROM `domains` WHERE `owner_id`=".$userId." ORDER BY `id` DESC";
      $userDomainsResult = $link->query($userDomainsQuery);
      $userDomainsCount = mysqli_num_rows($userDomainsResult);
    }
    if ($entryId > 0) {
      $equery = "SELECT * FROM `domains` WHERE `id`=".$entryId;
      $eresult = $link->query($equery);
      $erow = $eresult->fetch_assoc();
    }
    break;
  case '':
  case 'overview':
  case 'edit':
    if ($entryId > 0) {
      $equery = "SELECT * FROM `domains` WHERE `id`=".$entryId.$orderQuery;
      $eresult = $link->query($equery);
      $erow = $eresult->fetch_assoc();
    }
    break;
  case 'update':
  case 'updatedomain':
    if ($entryId > 0) {
      $equery = "SELECT * FROM `domains` WHERE `id`=".$entryId;
      $eresult = $link->query($equery);
      $erow = $eresult->fetch_assoc();
      $cquery = "SELECT COUNT(*) as total FROM `domains` WHERE `domain_name`='".$domain."' && `id`!='".$entryId."'";
      $cresult = $link->query($cquery);
      $crow = $cresult->fetch_assoc();
      if ($crow['total'] < 1) {
        $query = "UPDATE `domains` SET `domain_name`='".$domain."' WHERE `id`=".$entryId;
        $result = $link->query($query);
        if ($action == "update") {
          header('Location: '.ADMIN_HOME."?action=overview&sort=".$sort."&order=".$order);
        }else{
          header('Location: '.ADMIN_HOME."?action=detail&userid=".$userId);
        }
      }else{
        header("Location: ".ADMIN_HOME."?action=overview&msg=err_exists&sort=".$sort."&order=".$order);
      }
    }
    break;
  case 'edituser':
    if ($userId>0) {
      $userDomainsQuery = "SELECT * FROM `domains` WHERE `owner_id`=".$userId." ORDER BY `id` DESC";
      $userDomainsResult = $link->query($userDomainsQuery);
      $userDomainsCount = mysqli_num_rows($userDomainsResult);
    }
    if ($userId > 0) {
      $euquery = "SELECT * FROM `users` WHERE `id`=".$userId;
      $euresult = $link->query($euquery);
      $eurow = $euresult->fetch_assoc();
    }
    break;
  case 'updateuser':
    if ($userId > 0) {
      $valid_till = time() + $valid_till;
      if (trim($password) != "") {
        $pwd = md5( $username . $password );
        $pwdQuery = ", `password`='".$pwd."'";
      }
      $uuquery = "UPDATE `users` SET `name`='".$full_name."', `user_level`=".$level.", `max_domains`=".$max_domains.", `sub_length`=".$sub_length.", `valid_till`=".$valid_till.$pwdQuery." WHERE `id`=".$userId;
      $uuresult = $link->query($uuquery);
      header('Location: '.ADMIN_HOME."?action=detail&userid=".$userId);
    }
    break;
  case 'toggleuser':
    if ($userId > 0) {
      $tquery = "SELECT * FROM `users` WHERE `id`=".$userId;
      $tresult = $link->query($tquery);
      $trow = $tresult->fetch_assoc();
      $newactive = ($trow['status'] > 1) ? 0 : 2;
      $toggleQuery = "UPDATE `users` SET `status`=".$newactive." WHERE `id`=".$userId;
      $toggleResult = $link->query($toggleQuery);
      header('Location: '.ADMIN_HOME."?action=detail&userid=".$userId);
    }
    break;
  case 'toggle':
  case 'toggledomain':
    $equery = "SELECT * FROM `domains` WHERE `id`=".$entryId;
    $eresult = $link->query($equery);
    $erow = $eresult->fetch_assoc();
    $newactive = 1-$erow['status'];
    // $newactive = ($newactive > 2) ? 0 : $newactive;
    $query = "UPDATE `domains` SET `status`=".$newactive." WHERE `id`=".$entryId;
    $result = $link->query($query);
    if ($action == "toggledomain") {
      header('Location: '.ADMIN_HOME."?action=detail&userid=".$userId);
    }else{
      header('Location: '.ADMIN_HOME."?action=overview&sort=".$sort."&order=".$order);
    }
    break;
  case 'insert':
    if ($domain !="") {
      $domainCheck = "SELECT COUNT(*) as total FROM `domains` WHERE `domain_name`='".$domain."'";
      $dchResult = $link->query($domainCheck);
      $dres = $dchResult->fetch_assoc();
      if ($dres['total'] < 1) {
        $uid = md5($domain.time());
        $query = "INSERT INTO `domains` (owner_id, domain_name, time_added, uid, status) VALUES (".$_SESSION['user_id'].", '".$domain."', ".time().", '".$uid."', 1)";
        $result = $link->query($query);
      }else{
        header('Location: '.ADMIN_HOME."?action=overview&msg=err_exists&sort=".$sort."&order=".$order);
      }
    }
    header('Location: '.ADMIN_HOME."?action=overview&sort=".$sort."&order=".$order);
    break;
  case 'delete':
    if ($entryId > 0) {
      $equery = "SELECT * FROM `domains` WHERE `id`=".$entryId;
      $eresult = $link->query($equery);
      $erow = $eresult->fetch_assoc();
      $deleteHitsQuery = "DELETE FROM `hits` WHERE `expected_domain`='".$erow['domain_name']."'";
      $dhres = $link->query($deleteHitsQuery);
      $deleteQuery = "DELETE FROM `domains` WHERE `id`=".$entryId;
      $dres = $link->query($deleteQuery);
    }
    header('Location: '.ADMIN_HOME."?action=overview&sort=".$sort."&order=".$order);
    break;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cookies For Good</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel='shortcut icon' type='image/x-icon' href='../cookie.ico' />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="../assets/css/main.css">
  <link rel="stylesheet" href="../assets/css/cookies_font.css" type="text/css" />
  <link rel="stylesheet" href="../assets/css/admin.css" type="text/css" />
  <script src="../assets/js/admin.js"></script>
</head>
<body>
  <header>
    <img src="../assets/img/WITH_Icon_RGB.png" alt="Cookies For Good | WiTH Collective" />
    <img src="../assets/img/cookie.svg" alt="Cookie!!!!!" />
    <h1>Cookies For Good - Administration</h1>
    <div class="user-block">
      <span><?=$_SESSION['user_name'];?></span>
      <a href="?action=logout"><i class="icon-logout"></i> Logout</a>
    </div>
  </header>
  <content class="dashboard">
    <?php
      $file = ($_SESSION['user_level'] < 2) ? 'inc/user_dashboard.php' : 'inc/admin_dashboard.php';
      include($file);
    ?>
  </content>
</body>
</html>
