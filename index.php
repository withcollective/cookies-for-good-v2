<?php
session_start();
require('inc/_config.php');

$username = $_POST['username'];
$password = $_POST['password'];

if($username != "" && $password != ""){
  $pwd = md5( $username . $password );

  $query = "SELECT * FROM `users` WHERE `username`='".$username."' && `password`='".$pwd."' && `status`=2";
  $uResult = $link->query($query);
  $urow = $uResult->fetch_assoc();
  if ($urow['id'] > 0) {
    $_SESSION['user_id'] = $urow['id'];
    $_SESSION['user_level'] = $urow['user_level'];
    $_SESSION['user_name'] = $urow['name'];
    $_SESSION['user_uid'] = $urow['uid'];
    $_SESSION['last_login'] = $urow['last_login'];
    // Last Login query.
    $llquery = "UPDATE `users` SET `last_login`=".time()." WHERE `id`=".$urow['id']." && `uid`='".$urow['uid']."'";
    $llres = $link->query($llquery);
    header('Location: /admin/');
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cookies For Good</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel='shortcut icon' type='image/x-icon' href='cookie.ico' />
  <link rel="stylesheet" type="text/css" media="screen" href="assets/css/main.css">
  <link rel="stylesheet" href="assets/css/cookies_font.css" type="text/css" />
  <link rel="stylesheet" href="assets/css/admin.css" type="text/css" />
</head>
<body>
  <header>
    <img src="assets/img/WITH_Icon_RGB.png" alt="Cookies For Good | WiTH Collective" />
    <img src="assets/img/cookie.svg" alt="Cookie!!!!!" />
    <h1>Cookies For Good - Login</h1>
  </header>
  <div class="content flex-centered" style="padding:25px;">
    <form action="" method="post" style="margin-top:0;max-width:360px;">
      <div class="form-group">
        <label><span>Username</span><input type="text" name="username" required></label>
      </div>
      <div class="form-group">
        <label><span>Password</span><input type="password" name="password" autocomplete="yes" required></label>
      </div>
      <div class="form-group button-group flexrow">
        <button type="submit" class="btn left"><i class="icon-login"></i> Log in</button>
        <button type="button" class="btn right outline" onclick="document.location='signup.php'"><i class="icon-user-plus"></i> Sign up</button>
      </div>
    </form>
  </div>
</body>
</html>
