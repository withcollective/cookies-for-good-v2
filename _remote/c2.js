// var scriptBaseURL = "https://cookies.withcollective.com";
var scriptBaseURL = "http://cookiesnew";

readCookie = (qry) => {
  var allcookies = document.cookie;
  cookiearray = allcookies.split(';');
  for(var i=0; i<cookiearray.length; i++) {
    name = cookiearray[i].split('=')[0].trim();
    value = cookiearray[i].split('=')[1]*1;
    
    if (name == qry && value == 1) {
      return 1; 
    }
  }
  return 2;
}
writeCookie = () => {
  document.cookie = "gdpr=1";
}
if(readCookie("gdpr") == 2) {
  let dq = document.querySelector("script[src*='/c2.js']").dataset.id;
  let aq = document.querySelector("script[src*='/c2.js']").dataset.id2;
  let vars = "id="+dq+"&id2="+aq+"&host="+location.host;
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      // console.log('RESPONSE >>>>',this.responseText);
      if(this.responseText == "shoot!") {
        let disclaimer = document.createElement("div");
        disclaimer.classList.add('cookie-disclaimer-wrapper');
        disclaimer.innerHTML = '<button class="accept">I accept</button><p>...DISCLAIMER CONTENT GOES HERE...</p>';
        document.body.appendChild(disclaimer);
        disclaimer.querySelector('.accept').addEventListener('click', function(){
          disclaimer.parentElement.removeChild(disclaimer);
          writeCookie();
        });
      }else if (this.responseText == "__INACTIVE_DOMAIN") {
        let disclaimer = document.createElement("div");
        disclaimer.classList.add('cookie-disclaimer-wrapper','error');
        disclaimer.innerHTML = '<p>Ooops, your domain is marked as inactive. Please contact the administrators at <a style="color:#fff" href="mailto:admin@cookiesforgood.com.au">admin@cookiesforgood.com.au</a>.</p>';
        document.body.appendChild(disclaimer);
      }else if (this.responseText == "__EXPIRED_DOMAIN") {
        let disclaimer = document.createElement("div");
        disclaimer.classList.add('cookie-disclaimer-wrapper','error');
        disclaimer.innerHTML = '<p>Ooops, your domain has expired. Please contact the administrators at <a style="color:#fff" href="mailto:admin@cookiesforgood.com.au">admin@cookiesforgood.com.au</a>.</p>';
        document.body.appendChild(disclaimer);
      }else if (this.responseText == "__ENTRY_NOT_FOUND") {
        let disclaimer = document.createElement("div");
        disclaimer.classList.add('cookie-disclaimer-wrapper','error');
        disclaimer.innerHTML = '<p>This domain has not been found in our records. Please register it at <a target="_blank" style="color:#fff" href="https://cookies.withcollective.com">cookies.withcollective.com</a>.</p>';
        document.body.appendChild(disclaimer);
      }else if (this.responseText == "__PENDING_DOMAIN") {
        let disclaimer = document.createElement("div");
        disclaimer.classList.add('cookie-disclaimer-wrapper','error');
        disclaimer.innerHTML = '<p>This domain is pending activation. Please check your email.</p>';
        document.body.appendChild(disclaimer);
      }else if (this.responseText == "__MISMATCHED_DOMAINS") {
        let disclaimer = document.createElement("div");
        disclaimer.classList.add('cookie-disclaimer-wrapper','error');
        disclaimer.innerHTML = '<p>The referring domain does not match our records. Please contact the administrators at <a style="color:#fff" href="mailto:admin@cookiesforgood.com.au">admin@cookiesforgood.com.au</a></p>';
        document.body.appendChild(disclaimer);
      }
    }
  };
  xmlhttp.open("POST", scriptBaseURL+"/query.php", true);
  xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xmlhttp.send(vars);
}
