<?php 
require('inc/_config.php');

$username = clean($_POST['username']);
$first_name = clean($_POST['first_name']);
$last_name = clean($_POST['last_name']);
$valid_till = clean($_POST['valid_till']);
$sub_length = clean($_POST['sub_length']);
$max_domains = clean($_POST['max_domains']);
$address = clean($_POST['address']);
$email = clean($_POST['email']);
$phone = clean($_POST['phone']);
$password = clean($_POST['password']);
$password2 = clean($_POST['password2']);
$agreement = clean($_POST['agreement']);

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

if($username != "" && $password != ""){
  $name = $first_name." ".$last_name;
  $valid_till = time() + $valid_till;
  $pwd = md5( $username . $password );
  $uid = md5( $username . $password . time());

  $mail = new PHPMailer();
  $mail->IsSMTP();
  $mail->IsHTML(true);
  $mail->Host = "email-smtp.us-east-1.amazonaws.com";
  $mail->SMTPAuth = true;
  $mail->Username = 'AKIAJEDQHOV5MACKEXBA';
  $mail->Password = 'AkrG42paHKN5kt2zZaLZoBmLswT9nkjteNeXMTgge/ln';
  $mail->setFrom('register@withcollective.com', 'Cookies For Life', 0);
  $mail->addAddress($email, $name);
  $mail->Subject = 'User activation | Cookies For Life';

  $query = "INSERT INTO `users` (uid, username, password, name, email, phone, address, valid_till, time_created, user_level, max_domains, sub_length, status, last_login)
            VALUES ('".$uid."', '".$username."', '".$pwd."', '".$name."', '".$email."', '".$phone."', '".$address."', ".$valid_till.", ".time().", 1, ".$max_domains.", ".$sub_length.", 1, 0)";

  if ($insertResult = $link->query($query)) {
    $mail->Body = 'Hello '.$name.',<br><br>Thank you for signing up for Cookies Foor Good.<br><br>Please activate your account by clicking <a href="http://cookiesnew/activate.php?uid='.$uid.'">this link</a> or visit this URL:<br><br><span style="font-family:Courier">https://cookies.withcollective.com/activate.php?uid='.$uid.'</span><br><br>Thank you!<br><br>Kind regards,<br>Cookies For Good Team';
    if (!$mail->send()){
      die;
    }
    header('Location: /signup-followup.html');
  }else{
    header('Location: /signup.php');
  }
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cookie Machine</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel='shortcut icon' type='image/x-icon' href='cookie.ico' />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <script src="assets/js/main.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="assets/css/main.css">
  <link rel="stylesheet" href="assets/css/cookies_font.css" type="text/css" />
  <link rel="stylesheet" href="assets/css/admin.css" type="text/css" />
</head>
<body>
  <header>
    <img src="assets/img/WITH_Icon_RGB.png" alt="Cookies For Good | WiTH Collective" />
    <img src="assets/img/cookie.svg" alt="Cookie!!!!!" />
    <h1>Cookies For Good</h1>
  </header>



  <section class="signup">
  <form id="form" name="form" method="post" action="" onsubbmit="javascript:return false;">
    <div class="form-group dual">
      <label><span>First name(s)</span> <input required type="text" name="first_name" placeholder="" /></label>
      <label><span>Last name</span> <input required type="text" name="last_name" placeholder="" /></label>
    </div>
    <div class="form-group">
      <label><span>Username</span> <input required type="text" name="username" /></label>
    </div>
    <div class="form-group dual">
      <label><span>Subscription type</span>
        <select name="valid_till" class="select2rep select_period">
          <option value="15552000" rel="6">6 months</option>
          <option value="31536000" rel="12" selected>1 year</option>
          <option value="63072000" rel="24">2 years</option>
          <option value="999999999" rel="99999">permanent</option>
        </select>
      </label>
      <input type="hidden" id="sublen" name="sub_length" value="12" />

      <label><span>Max domains</span>
        <select name="max_domains" class="select2rep select_max">
          <option value="5">5</option>
          <option value="15">15</option>
          <option value="25">25</option>
          <option selected value="50">50</option>
          <option value="100">100</option>
          <option value="9999">100+</option>
        </select>
      </label>
    </div>
    <div class="form-group">
      <label><span>Billing address</span> <input required type="text" name="address" /></label>
    </div>
    <div class="form-group dual">
      <label><span>Email</span> <input required type="email" name="email" placeholder="e.g. john.doe@email.com.au" /></label>
      <label><span>Phone</span> <input required type="text" name="phone" placeholder="e.g. 0245637623" /></label>
    </div>
    <div class="form-group dual">
      <label><span>Password</span> <input required type="password" name="password" id="password" placeholder="" /></label>
      <label><span>Confirm password</span> <input required type="password" name="password2" id="password2" placeholder="" /></label>
    </div>
    <div class="form-group">
      <label><input type="checkbox" name="agreement" checked="false"/> <span>I agree to the <a href="#">Terms and conditions</a></span></label>
    </div>
    <div class="form-group button-group">
      <input type="submit" value="Sign up" onbclick="signUp()" disabled />
      <a class="btn" style="max-width:240px;float:right;" href="/index.php"><i class="icon-login"></i> I remembered...</a>
      <div id="loader" style="display:none">
        <img src="assets/img/loader.gif" alt="working..."/>
      </div>
    </div>
    <div id="errors"></div>
    <div id="success"></div>
  </form>
  </section>
</body>
</html>
